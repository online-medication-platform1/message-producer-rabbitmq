package com.dsproject.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

public class App 
{
    private final static String QUEUE_NAME = "monitored_data";
    public static void main( String[] args ) throws FileNotFoundException, ParseException {
        File file = new File("activity.txt");
        Scanner sc = new Scanner(file);
        ArrayList<PatientData> patientDataArrayList = new ArrayList<>();

        UUID patient_id = null;
        boolean isFirstLine = true;
        while (sc.hasNextLine()) {
            if(isFirstLine){
                patient_id = UUID.fromString(sc.nextLine());
                System.out.println((patient_id));
                isFirstLine=false;
            }else{
                String[] arrStr = sc.nextLine().split("\t\t");
                Date start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrStr[0]);
                Date end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrStr[1]);
                String activity = arrStr[2].replace("\t", "");
                PatientData patientData = new PatientData(patient_id, activity, start, end);
                //System.out.println(start.getTime() + " " + end.getTime() + " " + activity);
                patientDataArrayList.add(patientData);
            }
        }

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("roedeer.rmq.cloudamqp.com");
        factory.setUsername("ojqubljj");
        factory.setPassword("qMLAutllHwzUtqYv8NiYqwfKffMia3qZ");
        factory.setVirtualHost("ojqubljj");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            ObjectMapper mapper = new ObjectMapper();

            for(PatientData patientData: patientDataArrayList){
                String jsonMessage = mapper.writeValueAsString(patientData);
                channel.basicPublish("", QUEUE_NAME, null, jsonMessage.getBytes());
                System.out.println(" [x] Sent '" + jsonMessage + "'");
            }


        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

