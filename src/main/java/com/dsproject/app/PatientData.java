package com.dsproject.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class PatientData {

    private UUID patient_id;
    private String activity;
    private Date start;
    private Date end;

    public PatientData(UUID patient_id, String activity, Date start, Date end) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public UUID getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(UUID patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
